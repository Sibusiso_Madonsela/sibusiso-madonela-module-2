void main() {
  final app = App(
      name: 'Ambani Africa App',
      category: 'Gaming and Educatinal',
      developer: 'Mukundi Lambani',
      year: 2017);
  print(app.name);
  print(app.Upper());
}

//a) Creating a class
class App {
  App(
      {required this.name,
      required this.category,
      required this.developer,
      required this.year});
  late String name;
  late String category;
  late String developer;
  late int year;
//b) Creating a method that transform name to capital latters
  String Upper() {
    return name.toUpperCase();
  }
}
