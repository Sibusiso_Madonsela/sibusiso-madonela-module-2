import 'dart:convert';

void main() {
  // ignore: deprecated_member_use
  var winning_app_list = [
    "FNB Banking",
    "Health ID, TransUnion",
    "Rapidtarget",
    "Matchy",
    "Plascon",
    "PhraZApp",
    "Bookly",
    "MarkitShare",
    "DStv",
    ".comm Telco Data Visualize",
    "SnapScan",
    "Kids Aid",
    "Nedbank App Suite",
    "Price Check",
    "Gautrain Buddy",
    'SuperSport',
    'SyncMobile',
    'My Belongs',
    'Live Aspect',
    'Vigo',
    'Zapper',
    'Rea Vaya',
    'Wildlife',
    'WumDrop',
    'DStv Now',
    'Vula Mobile',
    'CPUT Mobile',
    'EskomSePush',
    'M4JAM',
    'Domestly',
    'iKhokha',
    'HearZA',
    'Tuta-me',
    'KaChing',
    'Friendly Math Monsters for Kindergarten',
    'TransUnion Check',
    'OrderIN',
    'Ecoslip',
    'InterGreatMe',
    'Zulzi',
    'HeyJude',
    "Pick n Pay's Super Animal 2",
    'TreeApp South Africa',
    'Watlf Health Portal',
    'Anethu Project',
    'Shyft for StandardBank',
    'Pinapple',
    'Cowa Bunga',
    'Digemy Knowledge Parter and Besmaler',
    'Bestee',
    'Africa Cyber Gaming League',
    'dbTrack',
    'Stockfella',
    'Difela Hymns',
    'Xander English 1-2',
    'Ctrl',
    'Khula',
    'ASI Snakes',
    'SI Realities',
    'Lost Defence',
    'Franc',
    'Vula Mobile',
    'Matric Live',
    'My Pregnancy Journal and Loctra',
    'LocTransie',
    'Hydra',
    'Bottles',
    'Over',
    'Digger',
    'Mo Wash',
    'EasyEquities',
    'Examsta',
    'Checkers Sixty60',
    'Technishen',
    'BirdPro',
    'Lexie Hearing',
    'League of Legends',
    'GreenFingers Mobile',
    'Stockfella',
    'Xitsonga Dictionary',
    'Bottle',
    'Matric Live',
    'Guardian Health',
    'My Pregnancy Journey',
    'iiDentifii',
    'HelloSoft',
    'Guardian Helath Platform',
    'Murimi',
    'Ambani Africa',
    'Shyft',
    'Sisa',
    'UniWise',
    'Kazi',
    'Takealot App',
    'Rekindle Learning App',
    'Roadsare',
    'Afrihost'
  ];
  winning_app_list.sort();
  print(winning_app_list); //Printing the list of winning apps from 2012-1021
  //Created a list of two app that could each be winning app for either 2017 or 2018
  var winningApp2017and2018 = ['OrderIN', 'Cowa Bunga'];
  int year = 2017;
  var equal2 = false;

  for (var i in winningApp2017and2018) {
    for (var z = 0; z < winning_app_list.length; z++) {
      if (i == winning_app_list[z]) {
        equal2 = true;
      }
    }
    if (equal2) {
      print('$i is the winning app for $year');
    } else {
      print('There is no winning app for these years');
    }
    year = year + 1; //increment year with 1 since we want an output for each
    //year
  }

  //Calculating the total number of items in winning_app_list
  int total_items = 0;
  for (var x in winning_app_list) {
    total_items = total_items + 1;
  }
  print('The total number of winning Apps from 2012 to 2021 is $total_items');
}
